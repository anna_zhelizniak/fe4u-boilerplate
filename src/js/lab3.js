const _ = require('lodash');

export function getFromRandom(n) {
  const url = `https://randomuser.me/api/?results=${n}`;
  const param = {
    method: 'GET',
    cache: 'no-cache',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
  };
  return fetch(url, param)
    .then((resp) => resp.json())
    .catch((error) => console.error(error));
}

export function createData(users) {
  const randomUsers = [];
  const subjects = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry',
    'Law', 'Art', 'Medicine', 'Statistics'];
  for (let i = 0; i < users.length; i += 1) {
    const current = users[i];
    randomUsers[i] = {
      gender: current.gender,
      title: current.name.title,
      full_name: `${current.name.first} ${current.name.last}`,
      city: current.location.city,
      state: current.location.state,
      country: current.location.country,
      postcode: current.location.postcode,
      coordinates: current.location.coordinates,
      timezone: current.location.timezone,
      email: current.email,
      b_date: current.dob.date,
      id: current.id.value,
      age: current.dob.age,
      phone: current.phone,
      picture_large: current.picture.large,
      picture_thumbnail: current.picture.thumbnail,
      favorite: Boolean(Math.floor(Math.random() * 2)),
      course: subjects[(Math.floor(Math.random() * subjects.length))],
      bg_color: '#ffffff',
      note: 'Some note here',
    };
  }
  return randomUsers;
}

export function validateUser(user) {
  const checkType = _.isString(user.full_name)
    && _.isString(user.gender)
    && _.isString(user.note)
    && (_.isString(user.state) || _.isUndefined(user.state))
    && _.isString(user.city)
    && _.isString(user.country)
    && (_.isNumber(user.age) || _.isUndefined(user.age));

  let checkCapital = false;
  if (checkType) {
    checkCapital = user.full_name === _.upperFirst(user.full_name)
      && user.gender === _.upperFirst(user.gender)
      && (user.note === _.upperFirst(user.note) || _.isNull(user.note))
      && user.state === _.upperFirst(user.state)
      && user.city === _.upperFirst(user.city)
      && user.country === _.upperFirst(user.country);
  }
  const checkPhone = /^\+380\d{9}$/.test(user.phone);
  const checkEmail = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(user.email);

  return checkType && checkCapital && checkPhone && checkEmail;
}

function predicateAge(value, condition) {
  switch (condition) {
    case '18-31':
      return value >= 18 && value <= 31;

    case '31-44':
      return value >= 31 && value <= 44;

    case '44-57':
      return value >= 44 && value <= 57;

    case '57-70':
      return value >= 57 && value <= 70;

    case '70-83':
      return value >= 70 && value <= 83;

    case '83-96':
      return value >= 83 && value <= 96;

    default:
      return null;
  }
}

function predicatePhoto(value) {
  return !(typeof value === 'undefined' || value == null);
}

export function filterUsers(users, param, condition) {
  if (condition === 'All') return users;

  switch (param) {
    case 'country':
      return _.filter(users, { country: condition });
    case 'age':
      return _.filter(users, (user) => predicateAge(user.age, condition));
    case 'gender':
      return _.filter(users, { gender: condition });
    case 'photo':
      return _.filter(users, (user) => predicatePhoto(user.picture_large));
    case 'favorite':
      return _.filter(users, { favorite: true });
    default:
      return users;
  }
}

export function filterUsers1(
  users, conditionAge, conditionCountry, conditionSex, conditionPhoto, conditionFavorite,
) {
  const res = filterUsers(
    filterUsers(
      filterUsers(
        filterUsers(
          filterUsers(users, 'age', conditionAge),
          'country', conditionCountry,
        ),
        'gender', conditionSex,
      ), 'photo', conditionPhoto,
    ), 'favorite', conditionFavorite,
  );
  return res;
}

function sortUsersUp(users, param) {
  return _.sortBy(users, [param]);
}

export function sortUsers(users, param, direction) {
  if (direction) return sortUsersUp(users, param);
  return sortUsersUp(users, param)
    .reverse();
}

function findUser(users, param, value) {
  switch (param) {
    case 'full_name':
      return _.find(users, { full_name: value });
    case 'age':
      return _.find(users, { age: value });
    case 'note':
      return _.find(users, { note: value });
    default:
      return null;
  }
}

export function findBy(users, value) {
  const found = [findUser(users, 'full_name', value), findUser(users, 'age', parseInt(value, 10)), findUser(users, 'note', value)].filter((user) => typeof user !== 'undefined');
  return ((users.indexOf(found[0])));
}

// function findPercentage(users, param, curPredicate) {
//   const temp = filterUsers(users, param, curPredicate);
//   return (temp.length / users.length) * 100;
// }

export const onlyFavorites = function (value) {
  return value;
};
