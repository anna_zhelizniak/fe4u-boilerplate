// eslint-disable-next-line import/extensions,import/named
import Chart from 'chart.js/auto';

import {
  createData,
  // eslint-disable-next-line import/named
  filterUsers, filterUsers1, findBy, getFromRandom, onlyFavorites,
} from './lab3';

const _ = require('lodash');
const dayjs = require('dayjs');
const axios = require('axios').default;
const dayOfYear = require('dayjs/plugin/dayOfYear');

dayjs.extend(dayOfYear);

// eslint-disable-next-line import/extensions
require('./test-module.js');
require('../css/app.css');

/** ******** Your code here! *********** */
let currentUsers;

function generateTop(filteredUsers, startIndex) {
  const topTeachers = document.createElement('div');
  topTeachers.id = 'allTeachers';
  topTeachers.className = 'icon';
  let l;
  if ((startIndex + 10) < filteredUsers.length) {
    l = startIndex + 10;
  } else {
    l = filteredUsers.length;
  }
  for (let i = startIndex; i < l; i += 1) {
    const userFigure = document.createElement('figure');

    const userFigcaption = document.createElement('figcaption');
    const userName = document.createElement('h3');
    // eslint-disable-next-line prefer-destructuring
    userName.innerText = filteredUsers[i].full_name.split(' ')[0];
    const userSurname = document.createElement('h3');
    // eslint-disable-next-line prefer-destructuring
    userSurname.innerText = filteredUsers[i].full_name.split(' ')[1];
    const speciality = document.createElement('h4');
    speciality.innerText = filteredUsers[i].course;
    const country = document.createElement('p');
    country.innerText = filteredUsers[i].country;
    userFigcaption.append(userName, userSurname, speciality, country);

    const userImage = document.createElement('a');
    userImage.href = '#popup_info';
    userImage.innerHTML = `<img src="${filteredUsers[i].picture_large}" alt="${filteredUsers[i].full_name}"/>`;

    const userAlt = document.createElement('div');
    userAlt.innerHTML = `<span class="center_i"><a href="#popup_info" class="center_i_a">${filteredUsers[i].full_name.split(' ')[0].charAt(0)}.${filteredUsers[i].full_name.split(' ')[1].charAt(0)}</a></span>`;
    userAlt.className = 'alt_icon';
    const star = document.createElement('div');
    if (filteredUsers[i].favorite) {
      star.className = 'star';
      star.innerText = '★';
    }
    if (typeof filteredUsers[i].picture_large === 'undefined') {
      userFigure.append(userAlt, star, userFigcaption);
    } else {
      userFigure.append(userImage, star, userFigcaption);
    }
    topTeachers.appendChild(userFigure);
    userFigure.addEventListener('click', () => {
      // eslint-disable-next-line no-use-before-define
      createPopup(i, '#teachers', filteredUsers, startIndex);
    });
  }
  document.getElementById('teachers')
    .replaceChild(topTeachers, document.getElementById('allTeachers'));
}

function updateTop(startIndex = 0) {
  let photo;
  let fav;
  if (document.querySelector('.with_photos').checked) {
    photo = '';
  } else {
    photo = 'All';
  }
  if (document.querySelector('.only_favorites').checked) {
    fav = '';
  } else {
    fav = 'All';
  }
  const filtration = filterUsers1(currentUsers, document.getElementById('age_filter').value,
    document.getElementById('region_filter').value,
    document.getElementById('sex_filter').value,
    photo, fav);
  generateTop(filtration, startIndex);
  return filtration;
}

let startFavorite = 0;
let favorites;

function createPopup(i, link1, users, startIndex) {
  const popupTeacher = document.createElement('div');
  popupTeacher.className = 'popup2';
  popupTeacher.id = 'popup_info1';
  popupTeacher.innerHTML = '<header> <h1 class="header_of_popup1">Teacher Info</h1>'
    + `<a class="close" href="${link1}">&times;</a></header>'`
    + '<section id="main_card">'
    + '<img class="main_img"'
    + `src="${users[i].picture_large}"`
    + 'alt="Teacher\'s image"/>'
    + '<div id="info_card">'
    + `<p class="name_h">${users[i].full_name} <span class="info_star" id="info_star"></span></p>`
    + `<p class="sp">${users[i].course}</p>`
    + '<div class="contacts">'
    + `<p>${users[i].city}, ${users[i].country}</p>`
    + `<p>${users[i].age}, ${users[i].gender}</p>`
    + '<p id="days_to_birthday"></p>'
    + `<a href="mailto:${users[i].email}" class="email">${users[i].email}</a>`
    + `<p>${users[i].phone}</p>`
    + '</div></div></section>'
    + `<p class="d">${users[i].note}</p>`
    + '<div id="map" class="map" </div>';
  document.getElementById('popup_info')
    .replaceChild(popupTeacher, document.getElementById('popup_info1'));
  // eslint-disable-next-line no-use-before-define
  countDays(users[i]);
  // eslint-disable-next-line no-use-before-define
  createMap(users[i]);
  if (users[i].favorite) {
    document.getElementById('info_star').innerText = '★';
  } else {
    document.getElementById('info_star').innerText = '☆';
  }
  document.getElementById('info_star')
    .addEventListener('click', () => {
      if (document.getElementById('info_star').innerText === '★') {
        document.getElementById('info_star').innerText = '☆';
        // eslint-disable-next-line no-param-reassign
        users[i].favorite = false;
      } else {
        document.getElementById('info_star').innerText = '★';
        // eslint-disable-next-line no-param-reassign
        users[i].favorite = true;
      }
      updateTop(startIndex);
      startFavorite = 0;
      favorites = filterUsers(currentUsers, 'favorite', onlyFavorites);
      // eslint-disable-next-line no-use-before-define
      generateFavorites();
    });
}

function countDays(user) {
  const dContainer = document.getElementById('days_to_birthday');
  const date = dayjs(user.b_date);
  const today = dayjs();
  const difference = Math.abs(date.dayOfYear() - today.dayOfYear());
  dContainer.innerText = `Days to next birthday : ${difference}`;
}

function createMap(user) {
  // initialize the map on the "map" div with a given center and zoom
  // eslint-disable-next-line no-undef
  const map = L.map('map')
    .setView([user.coordinates.latitude, user.coordinates.longitude], 13);
  // eslint-disable-next-line no-undef
  L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  })
    .addTo(map);

  // eslint-disable-next-line no-undef
  L.marker([user.coordinates.latitude, user.coordinates.longitude])
    .addTo(map);
  document.getElementById('map')
    .append(map);
  console.log(user.coordinates.latitude);
  console.log(user.coordinates.longitude);
}

function createPopupNotFound() {
  const popupError = document.createElement('div');
  popupError.className = 'popup2';
  popupError.id = 'popup_info1';
  popupError.innerHTML = '<header> <h1 class="header_of_popup1">Teacher Info</h1>'
    + '<a class="close" href="#">&times;</a></header>\''
    + '<p>User not found</p>';
  document.getElementById('popup_info')
    .replaceChild(popupError, document.getElementById('popup_info1'));
}

function initFiltrationBlock() {
  document.getElementById('teacher_filter')
    .addEventListener('change', () => {
      // updateTop();
      // eslint-disable-next-line no-use-before-define
      generateTablePages(updateTop(), 1);
    });
}

function generateFavorites() {
  for (let i = 0; i < 5; i += 1) {
    let position = startFavorite + i;
    if (position > favorites.length - 1) position -= favorites.length;
    const userFigure = document.createElement('figure');
    userFigure.id = `fav${i}`;
    const userFigcaption = document.createElement('figcaption');
    const userName = document.createElement('h3');
    // eslint-disable-next-line prefer-destructuring
    userName.innerText = favorites[position].full_name.split(' ')[0];
    const userSurname = document.createElement('h3');
    // eslint-disable-next-line prefer-destructuring
    userSurname.innerText = favorites[position].full_name.split(' ')[1];
    const country = document.createElement('p');
    country.innerText = favorites[position].country;
    userFigcaption.append(userName, userSurname, country);

    const userImage = document.createElement('a');
    userImage.href = '#popup_info';
    userImage.innerHTML = `<img src="${favorites[position].picture_large}" alt="${favorites[position].full_name}"/>`;

    const userAlt = document.createElement('div');
    userAlt.innerHTML = `<span class="center_i"><a href="#popup_info" class="center_i_a">${favorites[position].full_name.split(' ')[0].charAt(0)}.${favorites[position].full_name.split(' ')[1].charAt(0)}</a></span>`;
    userAlt.className = 'alt_icon';
    if (typeof favorites[position].picture_large === 'undefined') {
      userFigure.append(userAlt, userFigcaption);
    } else {
      userFigure.append(userImage, userFigcaption);
    }
    document.getElementById('favorite_teachers')
      .replaceChild(userFigure, document.getElementById(`fav${i}`));
    // eslint-disable-next-line no-loop-func
    userFigure.addEventListener('click', () => {
      createPopup(position, '#favorites', favorites);
    });
  }
}

const buttonNext = document.getElementById('next');
buttonNext.addEventListener('click', () => {
  startFavorite += 1;
  if (startFavorite > favorites.length - 1) startFavorite = 0;
  generateFavorites();
});

const buttonPrevious = document.getElementById('previous');
buttonPrevious.addEventListener('click', () => {
  startFavorite -= 1;
  if (startFavorite < 0) startFavorite = favorites.length - 1;
  generateFavorites();
});

const searchButton = document.getElementById('search');
searchButton.addEventListener('click', () => {
  const toSearch = document.getElementById('to_search').value;
  if (findBy(currentUsers, toSearch) === -1) {
    console.log(findBy(currentUsers, toSearch));
    createPopupNotFound();
  } else {
    createPopup(findBy(currentUsers, toSearch), '#', currentUsers);
  }
});

function generateRandomColor() {
  const maxVal = 0xFFFFFF; // 16777215
  let randomNumber = Math.random() * maxVal;
  randomNumber = Math.floor(randomNumber);
  randomNumber = randomNumber.toString(16);
  const randColor = randomNumber.padStart(6, 0);
  return `#${randColor.toUpperCase()}`;
}

function generateColors(size) {
  const res = [];
  for (let i = 0; i < size; i += 1) {
    res[i] = generateRandomColor();
  }
  return res;
}

function configChart(name, data, id) {
  const data1 = {
    labels: data.labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: data.data,
        backgroundColor: generateColors(data.data.length),
      },
    ],
  };

  const config = {
    type: 'pie',
    data: data1,
    options: {
      responsive: false,
      plugins: {
        legend: {
          display: false,
        },
        title: {
          display: true,
          text: name,
        },
      },
    },
  };
  const wrapper = document.querySelector(id);
  console.log(wrapper);
  console.log(id);
  // eslint-disable-next-line no-new
  new Chart(wrapper, config);
  wrapper.style.height = '400px';
  wrapper.style.width = '400px';
}

function generateTable(users) {
  // eslint-disable-next-line no-undef
  const names = [...new Set(_.map(users, (u) => u.full_name))];
  // eslint-disable-next-line no-undef
  const namesCount = _.map(names, (name) => _.filter(users, { full_name: name }).length);
  configChart('Name', {
    labels: names,
    data: namesCount,
  }, '.chart_name');

  // eslint-disable-next-line no-undef
  const specialities = [...new Set(_.map(users, (u) => u.course))];
  // eslint-disable-next-line no-undef
  const specialitiesCount = _.map(specialities, (speciality) => _
    .filter(users, { course: speciality }).length);
  configChart('Speciality', {
    labels: specialities,
    data: specialitiesCount,
  }, '.chart_speciality');

  // eslint-disable-next-line no-undef
  const ages = [...new Set(_.map(users, (u) => u.age))];
  // eslint-disable-next-line no-undef
  const agesCount = _.map(ages, (age) => _.filter(users, { age }).length);
  configChart('Age', {
    labels: ages,
    data: agesCount,
  }, '.chart_age');

  // eslint-disable-next-line no-undef
  const genders = [...new Set(_.map(users, (u) => u.gender))];
  // eslint-disable-next-line no-undef
  const gendersCount = _.map(genders, (gender) => _
    .filter(users, { gender }).length);
  configChart('Gender', {
    labels: genders,
    data: gendersCount,
  }, '.chart_gender');

  // eslint-disable-next-line no-undef
  const countries = [...new Set(_.map(users, (u) => u.country))];
  // eslint-disable-next-line no-undef
  const countriesCount = _.map(countries, (country) => _
    .filter(users, { country }).length);
  configChart('Country', {
    labels: countries,
    data: countriesCount,
  }, '.chart_country');
  // const table = document.createElement('table');
  // table.id = 'statistics_table';
  // table.innerHTML = '<tr class="first_row">'
  //   + '<th id="th_name" class="first_th">Name <span class="hide">&#8595</span></th>'
  //   + '<th id="th_speciality" class="th_s">Speciality <span class="hide">&#8595</span></th>'
  //   + '<th id="th_age" class="th_s">Age <span class="hide">&#8595</span></th>'
  //   + '<th id="th_gender" class="th_s">Gender <span class="hide">&#8595</span></th>'
  //   + '<th id="th_country" class="th_s">Country <span class="hide">&#8595</span></th>'
  //   + '</tr>';
  //
  // for (let i = 0; i < 10; i += 1) {
  //   const position = startIndex + i;
  //   console.log(position);
  //   console.log(users);
  //   if (position > users.length - 1) {
  //     break;
  //   } else {
  //     table.innerHTML += '<tr>'
  //       + `<td class="left_align_cell">${users[position].full_name}</td>`
  //       + `<td>${users[position].course}</td>`
  //       + `<td>${users[position].age}</td>`
  //       + `<td>${users[position].gender}</td>`
  //       + `<td>${users[position].country}</td>`
  //       + '</tr>';
  //   }
  // }
  // document.getElementById('statistics')
  //   .replaceChild(table, document.getElementById('statistics_table'));
  // document.getElementById('th_name')
  //   .addEventListener('click', () => {
  //     generateTable(0, sortUsers(users, 'full_name', sortDirectionN));
  //     generateTop(sortUsers(users, 'full_name', sortDirectionN), 0);
  //     // eslint-disable-next-line no-use-before-define
  //     generateTablePages(sortUsers(users, 'full_name', sortDirectionN), 1);
  //     sortDirectionN = !sortDirectionN;
  //   });
  // document.getElementById('th_speciality')
  //   .addEventListener('click', () => {
  //     // currentUsers = sortUsers(currentUsers, 'speciality', sortDirectionS);
  //     generateTable(0, sortUsers(users, 'speciality', sortDirectionS));
  //     generateTop(sortUsers(users, 'speciality', sortDirectionS), 0);
  //
  //     // eslint-disable-next-line no-use-before-define
  //     generateTablePages(sortUsers(users, 'speciality', sortDirectionS), 1);
  //     sortDirectionS = !sortDirectionS;
  //   });
  // document.getElementById('th_age')
  //   .addEventListener('click', () => {
  //     // currentUsers = sortUsers(currentUsers, 'age', sortDirectionA);
  //     generateTable(0, sortUsers(users, 'age', sortDirectionA));
  //     generateTop(sortUsers(users, 'age', sortDirectionA), 0);
  //
  //     // eslint-disable-next-line no-use-before-define
  //     generateTablePages(sortUsers(users, 'age', sortDirectionA), 1);
  //     sortDirectionA = !sortDirectionA;
  //   });
  // document.getElementById('th_gender')
  //   .addEventListener('click', () => {
  //     // currentUsers = sortUsers(currentUsers, 'gender', sortDirectionG);
  //     generateTable(0, sortUsers(users, 'gender', sortDirectionG));
  //     generateTop(sortUsers(users, 'gender', sortDirectionG), 0);
  //     // eslint-disable-next-line no-use-before-define
  //     generateTablePages(sortUsers(users, 'gender', sortDirectionG), 1);
  //     sortDirectionG = !sortDirectionG;
  //   });
  // document.getElementById('th_country')
  //   .addEventListener('click', () => {
  //     // currentUsers = sortUsers(currentUsers, 'gender', sortDirectionC);
  //     generateTable(0, sortUsers(users, 'country', sortDirectionC));
  //     generateTop(sortUsers(users, 'country', sortDirectionC), 0);
  //     // eslint-disable-next-line no-use-before-define
  //     generateTablePages(sortUsers(users, 'country', sortDirectionC), 1);
  //     sortDirectionC = !sortDirectionC;
  //   });
}

function generateTablePages(users, current) {
  const numberOfPages = (users.length % 10 === 0)
    ? Math.floor(users.length / 10) : Math.floor(users.length / 10) + 1;
  const pagesBlock = document.createElement('div');
  pagesBlock.id = 'table_navigation';
  pagesBlock.className = 'table_pages';
  // for (let i = 1; i <= numberOfPages; i += 1) {
  //   pagesBlock.innerHTML += `<a class="table_link" href="#statistics">${i}</a>`;
  // }
  let fp = '';
  if (current - 2 <= 0) {
    for (let i = 1; i <= current; i += 1) {
      fp += `<a class="table_link" href="#top_teachers">${i}</a>`;
    }
  } else {
    if (current - 2 !== 1) {
      fp += '<a class="table_link" href="#top_teachers">1</a>';
      fp += '<a class="table_link" href="">...</a>';
    }
    for (let i = current - 2; i <= current; i += 1) {
      fp += `<a class="table_link" href="#top_teachers">${i}</a>`;
    }
  }
  if (current + 2 > numberOfPages) {
    for (let i = current + 1; i <= numberOfPages; i += 1) {
      fp += `<a class="table_link" href="#top_teachers">${i}</a>`;
    }
  } else {
    for (let i = current + 1; i <= current + 2; i += 1) {
      fp += `<a class="table_link" href="#top_teachers">${i}</a>`;
    }
    if (current + 2 !== numberOfPages) {
      fp += '<a class="table_link" href="">...</a>';
      fp += `<a class="table_link" href="#top_teachers">${numberOfPages}</a>`;
    }
  }
  pagesBlock.innerHTML = fp;
  document.getElementById('teachers')
    .replaceChild(pagesBlock, document.getElementById('table_navigation'));
  // eslint-disable-next-line no-use-before-define
  initPages(users);
}

function initPages(users) {
  let allLinks = document.querySelectorAll('.table_link');
  for (let i = 0; i < allLinks.length; i += 1) {
    // eslint-disable-next-line no-loop-func
    allLinks[i].addEventListener('click', () => {
      allLinks = document.querySelectorAll('.table_link');
      if (allLinks[i].textContent !== '...') {
        // generateTable((parseInt(allLinks[i].textContent, 10) - 1) * 10, users);
        generateTop(users, (parseInt(allLinks[i].textContent, 10) - 1) * 10);
        generateTablePages(users, parseInt(allLinks[i].textContent, 10));
      }
    });
  }
}

// .addEventListener('click', () => {
// if (event.target.value !== '...') {
//   generateTable(parseInt(event.target.value, 10) * 10, currentUsers);
//   generateTablePages(currentUsers, parseInt(event.target.value, 10) );
// }
// });

function initPage() {
  getFromRandom(50)
    .then((resp) => {
      currentUsers = createData(resp.results);
      favorites = filterUsers(currentUsers, 'favorite', onlyFavorites);
      initFiltrationBlock();
      generateTop(currentUsers, 0);
      generateTable(currentUsers);
      generateTablePages(currentUsers, 1);
      generateFavorites();
      document.getElementById('form')
        .addEventListener('submit', (event) => {
          event.preventDefault();
          const currentDate = new Date();
          const date = new Date((event.target.date_of_birth.value));
          let sex;
          if (document.getElementById('male').checked) {
            sex = 'male';
          } else {
            sex = 'female';
          }
          let userAge = currentDate.getFullYear() - date.getFullYear();
          if (currentDate.getMonth() <= date.getMonth() && currentDate.getDay() < date.getDay()) {
            userAge -= 1;
          }
          if (userAge >= 18) {
            const u = {
              full_name: event.target.name.value,
              course: event.target.speciality.value,
              country: event.target.country.value,
              city: event.target.city.value,
              email: event.target.email.value,
              phone: event.target.telephone.value,
              b_date: event.target.date_of_birth.value,
              gender: sex,
              bg_color: event.target.background.value,
              note: event.target.comment.value,
              favorite: false,
              age: userAge,
            };
            axios.post('http://localhost:3000/teachers', u)
              .then(() => {
                currentUsers.push(u);
                updateTop();
                generateTable(updateTop());
                generateTablePages(updateTop(), 1);
              })
              .catch((er) => console.error(er));
          }
        });
    });
}

initPage();
